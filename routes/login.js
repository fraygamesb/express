import {Router} from "express";
import CryptoJS from "crypto-js";
import * as path from "path";
import fs from "fs";

const router = new Router()
router.get("/login", (req,res) => {
    res.render("login")
})
router.post("/login", async (req, res) => {
    console.log(req.body)
    const login = req.body.login
    const password = req.body.password
    const __dirname = path.resolve();
    const fileName = path.join(__dirname, 'data', 'users.json');
    try {
        const data = await fs.promises.readFile(fileName, 'utf8');
        const users = JSON.parse(data);
        const user = users.find((u) => u.login === login);
        if (!user) {
            return res.status(401).send('Неправильный пароль или логин');
        }
        console.log(user)
        const decryptedPassword = CryptoJS.AES.decrypt(user.password, 'secret-key').toString(CryptoJS.enc.Utf8);

        if (decryptedPassword !== password) {
            return res.status(401).send('Неправильный пароль или логин');
        }

        res.redirect("/rating");
    } catch (err) {
        console.error(err);
        return res.status(500).send('Ошибка сервера');
    }
})

export default router